export interface Book {
  id: number;
  title: string;
  content: string;
  authorId: number;
  categoryId: number;
}
