import Policy from '../policy'

const BooksPolicy = {
  ...Policy,

  create(user) {
   // console.log(user)
    return !!user
  },

  update(user, book) {
    console.log(user);
    console.log(book);
    //return user && user.id === book.authorId
    return true
  },

  destroy(user, book) {
    return user && (user.isAdmin || user.id === book.authorId)
  }
}

export default BooksPolicy
